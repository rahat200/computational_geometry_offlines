package structure;

import voronoi.Voronoi;

public class Arc extends SearchKey {
    private final Voronoi v;
    public BreakPoint left, right;
    public final Point site;

    public Arc(BreakPoint left, BreakPoint right, Voronoi v) {
        this.v = v;
        this.left = left;
        this.right = right;
        this.site = (left != null) ? left.s2 : right.s1;
    }

    public Arc(Point site, Voronoi v) {
        this.v = v;
        this.left = null;
        this.right = null;
        this.site = site;
    }

    protected Point getRightPoint() {
        if (right != null) return right.getPoint();
        return new Point(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
    }

    protected Point getLeftPoint() {
        if (left != null) return left.getPoint();
        return new Point(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
    }

    public Point getCircleCenter() {
        if ((this.left == null) 
        		|| (this.right == null)) 
        	return null;
        
        if (Point.counterClockWiseTurn(this.left.s1, this.site, this.right.s2) != -1) 
        	return null;
        return (this.left.getEdge().getIntersectPoint(this.right.getEdge()));
    }
}