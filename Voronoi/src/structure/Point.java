package structure;

import lib.DrawShape;


public class Point implements Comparable<Point> {
    public final double x, y;
    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public int compareTo(Point o) {
        if ((this.x == o.x) || (Double.isNaN(this.x) && Double.isNaN(o.x))) {
            if (this.y == o.y) {
                return 0;
            }
            return (this.y < o.y) ? -1 : 1;
        }
        return (this.x < o.x) ? -1 : 1;
    }


    public static Point midpoint(Point p1, Point p2) {
        double x = (p1.x + p2.x) / 2;
        double y = (p1.y + p2.y) / 2;
        return new Point(x, y);
    }


    public static int counterClockWiseTurn(Point a, Point b, Point c) {
    	//1 = counterclock
    	//-1 = clock
    	//0 = colinear
    	
        double area2 = (b.x-a.x)*(c.y-a.y) - (b.y-a.y)*(c.x-a.x);
        if      (area2 < 0) return -1;
        else if (area2 > 0) return +1;
        else                return  0;
    }

    public String toString() {
        return String.format("%.2f %.2f", this.x, this.y);
    }

    public double getDistance(Point point) {
        return Math.sqrt((this.x - point.x)*(this.x - point.x) 
        		+ (this.y - point.y)*(this.y - point.y));
    }

    public void draw() {
        DrawShape.setPenRadius(.01);
        DrawShape.point(x, y);
        DrawShape.setPenRadius();
    }

    
}
