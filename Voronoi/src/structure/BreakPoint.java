package structure;

import voronoi.Voronoi;



public class BreakPoint {
    private final Voronoi v;
    public final Point s1, s2; //2 site points at left and right of the breakpoint
    private VoronoiEdge vEdge;

    private double lastSweepY;
    private Point lastPoint;
    private boolean isLeft;
    public final Point startEdge;

    public BreakPoint(Point left, Point right, VoronoiEdge e, boolean isEdgeLeft, Voronoi v) {
        this.v = v;
        this.s1 = left;//site point
        this.s2 = right;
        this.vEdge = e;
        this.isLeft = isEdgeLeft;
        this.startEdge = this.getPoint();
    }

    
    public void setEdgeEndPoint(Point v) {
        if (isLeft) {
            this.vEdge.p1 = v;
        }
        else {
            this.vEdge.p2 = v;
        }
    }

    public void setEdgeEndPoint() {
        Point p = this.getPoint();
        if (isLeft) {
            this.vEdge.p1 = p;
        }
        else {
            this.vEdge.p2 = p;
        }
    }

    public Point getPoint() {
        double sLoc = v.getSweepLineY();
        
        if (sLoc == lastSweepY) {
            return lastPoint;
        }
        lastSweepY = sLoc;
        double x,y;
        
        if (s1.y == s2.y) {
            x = (s1.x + s2.x) / 2; // x coordinate is between the two sites
            //y = (((x - s1.x)*(x - s1.x)) + (s1.y*s1.y) - (l*l)) / (2* (s1.y - l));
            y = (square(x - s1.x) + square(s1.y) - square(sLoc)) / (2* (s1.y - sLoc));
        }
        else {
            double px = (s1.y > s2.y) ? s1.x : s2.x;
            double py = (s1.y > s2.y) ? s1.y : s2.y;
            double m = vEdge.m;
            double c = vEdge.c;
            double d = 2*(py - sLoc);
            double A = 1;
            double B = -2*px - d*m;
            double C = square(px) + square(py) - square(sLoc) - d*c;
            int s = (s1.y > s2.y) ? -1 : 1;
            
            double D = square(B) - 4 * A * C;
            
            if (D <= 0) 
                x = -B / (2 * A);
            else 
                x = (-B + s * Math.sqrt(D)) / (2 * A);
            
            y = m*x + c;
        }
        lastPoint = new Point(x, y);
        return lastPoint;
    }

    private static double square(double d) {
        return d * d;
    }


    public VoronoiEdge getEdge() {
        return this.vEdge;
    }
}
