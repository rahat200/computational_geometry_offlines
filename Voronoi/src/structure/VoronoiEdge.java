package structure;




public class VoronoiEdge {
    public final Point site1, site2;
    public final double m, c; 
    public final boolean isVertical;
    public Point p1, p2;

    public VoronoiEdge(Point site1, Point site2) {
        this.site1 = site1;
        this.site2 = site2;
        isVertical = (site1.y == site2.y) ? true : false;
        if (isVertical) m = c = 0;
        else {
            m = -1.0 / ((site1.y - site2.y) / (site1.x - site2.x));
            Point midpoint = Point.midpoint(site1, site2);
            c = midpoint.y - m*midpoint.x;
        }
    }

    public VoronoiEdge clone() {
    	VoronoiEdge cloned = new VoronoiEdge(p1, p2);
		
		return cloned;
	}
    
    public Point getIntersectPoint(VoronoiEdge ve) {
        if (this.m == ve.m && this.c != ve.c) return null; // no intersection
        double x, y;
        if (this.isVertical) {
            x = (this.site1.x + this.site2.x) / 2;
            y = ve.m*x + ve.c;
        }
        else if (ve.isVertical) {
            x = (ve.site1.x + ve.site2.x) / 2;
            y = this.m*x + this.c;
        }
        else {
            x = (ve.c - this.c) / (this.m - ve.m);
            y = m * x + c;
        }
        return new Point(x, y);
    }
}
