package structure;


public class GetArc extends SearchKey {
    private final Point p;
    public GetArc(Point p) {
        this.p = p;
    }
    protected Point getLeftPoint() {
        return p;
    }
    protected Point getRightPoint() {
        return p;
    }
}
