package structure;

public abstract class SearchKey implements Comparable<SearchKey> {
    protected abstract Point getLeftPoint();
    protected abstract Point getRightPoint();

    public int compareTo(SearchKey that) {
        
    	Point myLeft = this.getLeftPoint();
        Point myRight = this.getRightPoint();
        Point yourLeft = that.getLeftPoint();
        Point yourRight = that.getRightPoint();

        if (((that.getClass() == GetArc.class) || 
        		(this.getClass() == GetArc.class)) &&
            ((myLeft.x <= yourLeft.x && myRight.x >= yourRight.x) ||
                (yourLeft.x <= myLeft.x && yourRight.x >= myRight.x ))) {
            //System.out.println("arc same");
        	return 0;
        }

        if (myLeft.x == yourLeft.x && myRight.x == yourRight.x) //same
        	return 0;
        
        if (myLeft.x >= yourRight.x) //greater
        	return 1;
        
        if (myRight.x <= yourLeft.x) //smaller
        	return -1;

        return Point.midpoint(myLeft, myRight)
        		.compareTo(Point.midpoint(yourLeft, yourRight));
    }
}
