package voronoi;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import java.util.TreeSet;

import lib.DrawShape;
import structure.Arc;
import structure.SearchKey;
import structure.GetArc;
import structure.BreakPoint;
import structure.Point;
import structure.VoronoiEdge;


public class Voronoi {
    
    private final ArrayList<Point> sitePoints;
    private final ArrayList<VoronoiEdge> edgeList;
    
    private TreeMap<SearchKey, CircleEvent> beachline;
    private TreeSet<Event> eventsList;
    
    private HashSet<BreakPoint> breakPoints;
    private double sweepLineY;
    
	public static ArrayList<Point> input() {
		
		BufferedReader br = null;
		
		String file = "inputs/input1.txt";
		Random rand = new Random();
		ArrayList<Point> sites = new ArrayList<Point>();

		try {
			br = new BufferedReader(new FileReader(file));
			String line;
			
			while((line = br.readLine()) != null) {
				String[] values = line.trim().split("\\s+");
				
				double x = Double.parseDouble(values[0]);
			    double y = Double.parseDouble(values[1]);

//			    double x = rand.nextDouble();
//			    double y = rand.nextDouble();
			       
			    sites.add(new Point(x,y));
			}
			
		} catch (IOException e){
			e.printStackTrace();
		} finally {
			try {
				if (br != null) br.close();
			} catch (IOException e) {
			}
		}
		return sites;
	}
	
    
    
    
    public Voronoi(ArrayList<Point> sitePoints) {
        
        this.sitePoints = sitePoints;
        edgeList = new ArrayList<VoronoiEdge>(sitePoints.size());
        eventsList = new TreeSet<Event>();
        breakPoints = new HashSet<BreakPoint>();
        beachline = new TreeMap<SearchKey, CircleEvent>();

        for (Point site : sitePoints) {
            eventsList.add(new Event(site));
        }
        sweepLineY = 10;
        do {
            Event cur = eventsList.pollFirst();
            sweepLineY = cur.point.y;
            
            if (cur.getClass() == Event.class) {
                handleSitePointEvent(cur);
            }
            else {
                CircleEvent ce = (CircleEvent) cur;
                handleCircleEvent(ce);
            }
        } while ((eventsList.size() > 0));

        sweepLineY = -10; 
        for (BreakPoint bp : breakPoints) {
            bp.setEdgeEndPoint();
        }
    }

    private void handleSitePointEvent(Event current) {
        
        if (beachline.size() == 0) {
            beachline.put(new Arc(current.point, this), null);
            return;
        }

        // arc above
        Map.Entry<SearchKey, CircleEvent> arcabove = beachline.floorEntry(new GetArc(current.point));
        
        if(arcabove == null)
        	System.out.println("arc above null");
        
        Arc arcOnAbove = (Arc) arcabove.getKey();

        //System.out.println("arc above found");
        
        //delete as its a false alarm
        CircleEvent falseAlarm = arcabove.getValue();
        if (falseAlarm != null) {
            eventsList.remove(falseAlarm);
        }

        BreakPoint breakLeft = arcOnAbove.left;
        BreakPoint breakRright = arcOnAbove.right;
        
        VoronoiEdge newEdge = new VoronoiEdge(arcOnAbove.site, current.point);
        
//        if(newEdge == null)
//        	System.out.println("new edge null");
//        
        this.edgeList.add(newEdge);
        //intersection point with new arc and arcabove it 
        BreakPoint nBreakLeft = new BreakPoint(arcOnAbove.site, current.point, newEdge, true, this);
        BreakPoint nBreakRight = new BreakPoint(current.point, arcOnAbove.site, newEdge, false, this);
//      if(nBreakLeft == null||nBreakRight == null)
//    	System.out.println("break left right null");
        
        breakPoints.add(nBreakLeft);
        breakPoints.add(nBreakRight);

        Arc arcLeft = new Arc(breakLeft, nBreakLeft, this);
        Arc center = new Arc(nBreakLeft, nBreakRight, this);
        Arc arcRight = new Arc(nBreakRight, breakRright, this);
        
//        System.out.println(arcLeft.toString());
//        System.out.println(arcRight.toString());
//        System.out.println(center.toString());

        beachline.remove(arcOnAbove);
        beachline.put(arcLeft, null);
        beachline.put(center, null);
        beachline.put(arcRight, null);

        isCircleEvent(arcLeft);
        isCircleEvent(arcRight);
    }

    private void handleCircleEvent(CircleEvent circleevent) {
        Arc arcR = (Arc) beachline.higherKey(circleevent.arc);
        Arc arcL = (Arc) beachline.lowerKey(circleevent.arc);
        
        if (arcR != null) {
            CircleEvent fce = beachline.get(arcR);
            if (fce != null) 
            	eventsList.remove(fce);
            
            beachline.put(arcR, null);
        }
        if (arcL != null) {
            CircleEvent fce = beachline.get(arcL);
            if (fce != null) 
            	eventsList.remove(fce);
            
            beachline.put(arcL, null);
        }
        beachline.remove(circleevent.arc); //this arc dissappears now

        circleevent.arc.left.setEdgeEndPoint(circleevent.v);//voronoi vertex found, set end point of voronoi edge
        circleevent.arc.right.setEdgeEndPoint(circleevent.v);

        breakPoints.remove(circleevent.arc.left);
        breakPoints.remove(circleevent.arc.right);

        VoronoiEdge e = new VoronoiEdge(circleevent.arc.left.s1, circleevent.arc.right.s2);//new voronoi edge
        edgeList.add(e);

        //to select whether it is end point or start point
        boolean turn = Point.counterClockWiseTurn(arcL.right.startEdge, circleevent.point, arcR.left.startEdge) == 1;
        boolean isleft = (turn) ? (e.m < 0) : (e.m > 0);
        if (isleft) 
            e.p1 = circleevent.v;
        else 
            e.p2 = circleevent.v;
        
        BreakPoint newBP = new BreakPoint(circleevent.arc.left.s1, circleevent.arc.right.s2, e, !isleft, this);
        breakPoints.add(newBP);

        arcR.left = newBP;
        arcL.right = newBP;

        isCircleEvent(arcL);
        isCircleEvent(arcR);
    }

   
    private void drawDiagram() {
        DrawShape.clear();
        DrawShape.setPenColor(DrawShape.BLUE);
        for (Point p : sitePoints) {
        	
            p.draw();
            System.out.println(p);
        }
        DrawShape.setPenColor(DrawShape.RED);
        for (VoronoiEdge e : edgeList) {
            if (e.p1 != null && e.p2 != null) {
                double topY = (e.p1.y == Double.POSITIVE_INFINITY) ? 10 : e.p1.y; 
                DrawShape.line(e.p1.x, topY, e.p2.x, e.p2.y);
            }
        }
        DrawShape.show();
    }
    
    private void isCircleEvent(Arc a) {
        
    	Point circleCenter = a.getCircleCenter();//to check whether it creates a circle
        
        if (circleCenter != null) {
            double radius = a.site.getDistance(circleCenter);
            Point circleEventPoint = new Point(circleCenter.x, circleCenter.y - radius);
            CircleEvent ce = new CircleEvent(a, circleEventPoint, circleCenter);
            
            beachline.put(a, ce);
            eventsList.add(ce);
        }
    }

    
    public double getSweepLineY() {
        return sweepLineY;
    }


    public static void main(String[] args) {
        ArrayList<Point> sites = input();
        
        DrawShape.setCanvasSize(800, 600);
        
        Voronoi v = new Voronoi(sites);
        v.drawDiagram();
    
}






}

