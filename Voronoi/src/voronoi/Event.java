package voronoi;

import structure.Point;


public class Event implements Comparable<Event> {
    public final Point point;

    public Event(Point p) {
        this.point = p;
    }
    @Override
    public int compareTo(Event e) {
    	if (point.y < e.point.y) return 1;
        if (point.y > e.point.y) return -1;
        if (point.x == e.point.x) return 0;
        return (point.x < e.point.x) ? -1 : 1;
    }
}
