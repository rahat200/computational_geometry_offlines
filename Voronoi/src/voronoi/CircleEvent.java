package voronoi;

import structure.Arc;
import structure.Point;

public class CircleEvent extends Event {
    public final Arc arc;
    public final Point v;

    public CircleEvent(Arc a, Point p, Point v) {
        super(p);
        this.arc = a;
        this.v = v;
    }

}
