import java.awt.geom.Point2D;
import java.util.HashSet;

public class Vertex implements Comparable<Vertex> {
	private int x;
	private int y;
	
	private Type type;
	
	private Vertex prev;
	private Vertex next;
	
	private Edge prevEdge;
	private Edge nextEdge;
	
	public boolean isOnLeftChain;
	
	private Edge helpedEdge;
	private boolean helping;
	
	private HashSet<Edge> diagonals = null;

	public Vertex(int x, int y) {
		this.x = x;
		this.y = y;
	}


	public int getX() {
		return this.x;
	}

	public int getY() {
		return this.y;
	}

	public Type getVertexType() {
		return this.type;
	}

	public boolean isStartVertex() {
		return getVertexType() == Type.START;
	}

	public boolean isEndVertex() {
		return getVertexType() == Type.END;
	}

	public boolean isSplitVertex() {
		return getVertexType() == Type.SPLIT;
	}

	public boolean isMergeVertex() {
		return getVertexType() == Type.MERGE;
	}

	public boolean isRegularLeftVertex() {
		return getVertexType() == Type.REGULAR_LEFT;
	}

	public boolean isRegularRightVertex() {
		return getVertexType() == Type.REGULAR_RIGHT;
	}


	public Vertex getPrev() {
		return this.prev;
	}

	public void setPrev(Vertex prev) {
		this.prev = prev;
	}

	public Vertex getNext() {
		return this.next;
	}

	public void setNext(Vertex next) {
		this.next = next;
	}

	public Edge getPrevEdge() {
		return this.prevEdge;
	}

	public void setPrevEdge(Edge prevEdge) {
		this.prevEdge = prevEdge;
	}

	public Edge getNextEdge() {
		return this.nextEdge;
	}

	public void setNextEdge(Edge nextEdge) {
		this.nextEdge = nextEdge;
	}

	public boolean calculateVertexType() {
		
		
		if ((isLowerThan(this, getPrev()))
				&& (isLowerThan(getNext(), this))) {
			this.type = Type.REGULAR_LEFT;
		} else if ((isLowerThan(getPrev(), this))
				&& (isLowerThan(this, getNext()))) {
			this.type = Type.REGULAR_RIGHT;
		
		
		} else if ((isLowerThan(this, getPrev()))
				&& (isLowerThan(this, getNext()))) {
			//System.out.println("end merge");
			if (liesLeftOfLine(getPrevEdge(), getNext())) {
				this.type = Type.END;
			} else {
				//System.out.println("merge");
				this.type = Type.MERGE;
			}
		
		} else if ((isLowerThan(getPrev(), this))
				&& (isLowerThan(getNext(), this))) {
			//System.out.println("start split");
			if (liesLeftOfLine(getPrevEdge(), getNext())) {
				this.type = Type.START;
				//System.out.println("start");
			} else {
				//System.out.println("split");
				this.type = Type.SPLIT;
			}
		} else {
			System.out.println("No vertex type found");
			return false;
		}
		return true;
	}

	public void setHelpedEdge(Edge e) {
		this.helpedEdge = e;
		this.helping = true;
	}

	public void setNotHelping() {
		this.helpedEdge = null;
		this.helping = false;
	}

	public void addDiagonal(Edge e) {
		if (this.diagonals == null) {
			this.diagonals = new HashSet();
		}
		this.diagonals.add(e);
	}

	public HashSet<Edge> getDiagonals() {
		return this.diagonals;
	}

	public boolean hasDiagonals() {
		return (this.diagonals != null) && (this.diagonals.size() > 0);
	}
	
	
	public boolean isEqual(Vertex obj) {
		return (this.getX()==obj.getX() && this.getY()==obj.getY());
	}

	public int compareTo(Vertex other) {
		return isLowerThan(other, this) ? -1 : 1;
	}

	public Vertex clone() {
		Vertex cloned = new Vertex(getX(), getY());
		cloned.helpedEdge = this.helpedEdge;
		cloned.helping = this.helping;

		return cloned;
	}

	public String toString() {
		return " (" + this.x + ", " + this.y + ")";
	}

	public String printVertexType() {
		return this.toString()+ " ---"+getVertexType().toString();
	}


	public Point2D getPoint2D() {
		return new Point2D.Double(getX(), getY());
	}
	
	public boolean liesLeftOfLine(Edge e, Vertex v) {
		int eX = e.getEndVertex().getX() - e.getStartVertex().getX();
		int eY = e.getEndVertex().getY() - e.getStartVertex().getY();
		int mX = v.getX() - e.getStartVertex().getX();
		int mY = v.getY() - e.getStartVertex().getY();

		return eX * mY - eY * mX < 0;
	}
	public boolean isLowerThan(Vertex one, Vertex two) {
		if (one.getY() < two.getY()) {
			return false;
		}
		if (one.getY() > two.getY()) {
			return true;
		}
		return one.getX() > two.getX();
	}
}
