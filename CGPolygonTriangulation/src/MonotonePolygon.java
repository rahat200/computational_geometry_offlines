import java.awt.Polygon;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Stack;
import java.util.TreeSet;

public class MonotonePolygon {

	private TreeSet<Vertex> vertices;
	
	private LinkedList<Edge> edges;
	
	private HashSet<Edge> activeEdgeSet;
	
	private LinkedList<Edge> diagonals;

	private BinarySearchTree BSTree;

	public TreeSet<Vertex> getVertices() {
		return this.vertices;
	}

	public LinkedList<Edge> getDiagonals() {
		return this.diagonals;
	}

//	public LinkedList<Edge> getEdgess() {
//		return this.edges;
//	}

	public boolean initialize(Polygon p) {

		this.BSTree = new BinarySearchTree();
		this.diagonals = new LinkedList();
		this.activeEdgeSet = new HashSet();

		initVerticesAndEdges(p) ;
			

		if (!checkOrientation(this.vertices)) {
			// System.out.println("Polygon Orientation changing!");
			this.vertices = reverseOrientation(this.vertices);
		}

		if (!computeVertexType()) {
			return false;
		}
		for (Vertex v : this.vertices) {
			System.out.println(v);
		}
		for (Vertex v : this.vertices) {
			handleVertexEvent(v);
		}

		return true;
	}

	// start of handling

	private void handleVertexEvent(Vertex v) {
		if (v.isStartVertex()) {
			handleStartVertex(v);
		
		} else if (v.isEndVertex()) {
			handleEndVertex(v);
		
		} else if (v.isSplitVertex()) {
			handleSplitVertex(v);
		
		} else if (v.isMergeVertex()) {
			handleMergeVertex(v);
		
		} else if (v.isRegularRightVertex()) {
			handleRegularRightVertex(v);
		
		} else if (v.isRegularLeftVertex()) {
			handleRegularLeftVertex(v);
		}
	}

	private void handleStartVertex(Vertex v) {
		Edge nextEdge = v.getNextEdge();
		insertEdgeInTree(nextEdge);
		nextEdge.setHelper(v);
	}

	private void handleEndVertex(Vertex v) {
		Edge prevEdge = v.getPrevEdge();
		boolean helperIsMerge = prevEdge.getHelper().isMergeVertex();
		if (helperIsMerge) {
			addDiagonal(v, prevEdge);
		}
		deleteEdgeFromTree(prevEdge);
	}

	private void handleSplitVertex(Vertex v) {

		Edge leftOfVEdge = this.BSTree.searchEdge(v);
		addDiagonal(v, leftOfVEdge);
		leftOfVEdge.setHelper(v);

		Edge nextEdge = v.getNextEdge();
		insertEdgeInTree(nextEdge);
		nextEdge.setHelper(v);

	}

	private void handleMergeVertex(Vertex v) {
		Edge prevEdge = v.getPrevEdge();
		boolean helperIsMerge = prevEdge.getHelper().isMergeVertex();
		if (helperIsMerge) {
			addDiagonal(v, prevEdge);
		}
		deleteEdgeFromTree(prevEdge);

		Edge leftOfVEdge = this.BSTree.searchEdge(v);
		boolean helperIsMerge2 = leftOfVEdge.getHelper().isMergeVertex();
		if (helperIsMerge2) {
			addDiagonal(v, leftOfVEdge);
		}
		leftOfVEdge.setHelper(v);

	}

	private void handleRegularLeftVertex(Vertex v) {

		Edge prevEdge = v.getPrevEdge();
		boolean helperIsMerge = prevEdge.getHelper().isMergeVertex();
		if (helperIsMerge) {
			addDiagonal(v, prevEdge);
		}
		deleteEdgeFromTree(prevEdge);
		Edge nextEdge = v.getNextEdge();
		insertEdgeInTree(nextEdge);
		nextEdge.setHelper(v);

	}

	private void handleRegularRightVertex(Vertex v) {

		Edge leftOfVEdge = this.BSTree.searchEdge(v);
		boolean helperIsMerge = leftOfVEdge.getHelper().isMergeVertex();
		if (helperIsMerge) {
			addDiagonal(v, leftOfVEdge);
		}
		leftOfVEdge.setHelper(v);
	}

	private void addDiagonal(Vertex v, Edge edge) {
		Edge newDiagonal = new Edge(v, edge.getHelper());
		v.addDiagonal(newDiagonal);
		edge.getHelper().addDiagonal(newDiagonal);
		this.diagonals.add(newDiagonal);
	}

	private void deleteEdgeFromTree(Edge toDelete) {
		this.BSTree.delete(toDelete);
		this.activeEdgeSet.remove(toDelete);
	}

	private void insertEdgeInTree(Edge toInsert) {
		if (toInsert == null) {
			throw new IllegalArgumentException();
		}
		this.BSTree.insert(toInsert);
		this.activeEdgeSet.add(toInsert);
	}





	// end of handling

	private boolean initVerticesAndEdges(Polygon poly) {
		this.vertices = new TreeSet();
		this.edges = new LinkedList();

		int[] xPoints = poly.xpoints;
		int[] yPoints = poly.ypoints;

		Vertex first = new Vertex(xPoints[0], yPoints[0]);
		this.vertices.add(first);
		Vertex current = first;
		for (int i = 1; i < poly.npoints; i++) {
			Vertex prev = current;
			current = new Vertex(xPoints[i], yPoints[i]);
			current.setPrev(prev);
			prev.setNext(current);
			this.vertices.add(current);
		}
		first.setPrev(current);
		current.setNext(first);

		current = first;
		do {
			Edge e = new Edge(current, current.getNext());
			current.setNextEdge(e);
			current = current.getNext();
			current.setPrevEdge(e);
			edges.add(e);
		} while (current != first);
		return true;
	}

	private boolean computeVertexType() {
		for (Vertex v : this.vertices) {
			if (!v.calculateVertexType()) {
				return false;
			}
		}
		return true;
	}

	public ArrayList<Polygon> getMonotonePolygons() {
		TreeSet<Vertex> mvertices = (TreeSet<Vertex>) vertices.clone();
		ArrayList<Polygon> polygons = new ArrayList<Polygon>();
		//System.out.println("diagonals:");
		for(Vertex e: mvertices) {
			//System.out.println(""+e+" "+e.getNext());
		}
		
		for(Edge diagonal: diagonals) {
			Vertex dstartp = diagonal.getStartVertex();
			Vertex dendp = diagonal.getEndVertex();
			
			Vertex vstartp = null;
			Vertex vendp = null;
			//System.out.println(dstartp.getX()+" "+dstartp.getY());
			for(Vertex v: mvertices) {
				//System.out.println(v.getX()+" "+v.getY());
				if(v!=null) {
					if(v.isEqual(dstartp)){
						vstartp = v;
//						if(vstartp==null)
//							System.out.println("inn");
					}
					if(v.isEqual(dendp)){
						vendp = v;
						
					}
				}
			}
			if(vstartp== null || vendp==null) {
				System.out.println("Error: Diagonal point not in vertices list");
			}
			Polygon p = new Polygon();
			p.addPoint(vstartp.getX(), vstartp.getY());
			Vertex current = vstartp;
			
			
			while(!current.isEqual(vendp)) {
				current = current.getPrev();
				//System.out.println(current.toShortString());
				p.addPoint(current.getX(), current.getY());
				if(!current.isEqual(vendp)) {
					for(Iterator<Vertex> iter = mvertices.iterator(); iter.hasNext();) {
					    Vertex v = iter.next();
					    if (v.isEqual(current)) {
					        iter.remove();
					    }
					}
				}
				
			}
			vstartp.setPrev(vendp);
			vendp.setNext(vstartp);
			
			polygons.add(p);
			
		}
		Polygon po = new Polygon();
		for(Vertex v:mvertices) {
			po.addPoint(v.getX(), v.getY());
		}
		polygons.add(po);
		return polygons;
		
	}
	
	 public ArrayList<Triangle> triangulateMonotone(Polygon polygon){
		 
		 
		 PriorityQueue PQ = new PriorityQueue(30,new VertexComparator());
		 Stack stack = new Stack();
		 
		 int[] xPoints = polygon.xpoints;
		 int[] yPoints = polygon.ypoints;

		 int p_size = xPoints.length;
		 

		 Vertex vertex;    
		 Vertex nextvertex;
		 
		 for(int i = 0; i < p_size-1; i++) {
			 int next = i+1;
			 if (next == p_size) next = 0;
			 vertex     = new Vertex(xPoints[i],yPoints[i]);
			 nextvertex     = new Vertex(xPoints[next],yPoints[next]);
			 
			 vertex.isOnLeftChain = (vertex.compareTo(nextvertex) > 0) ? true : false;
			 
			 PQ.add(vertex);
		 }

		 
		 
		 for(int i = 0; i < 2; i++) 
			 stack.push(PQ.poll());

		 Vertex Qtop; 
		 Vertex Stop;
		 
		 
		 ArrayList<Triangle> triangleList = new ArrayList<Triangle>();
		
		 while ( PQ.size() > 1 ){
			 
			 Qtop = (Vertex)PQ.peek();
			 Stop = (Vertex)stack.peek();

			 if(Qtop.isOnLeftChain != Stop.isOnLeftChain){
				 
				 while ( stack.size()  > 1 ){
					 
					 Vertex v1 = (Vertex)stack.peek();
					 stack.pop(); 
					 Vertex v2 = (Vertex)stack.peek();
					 
					 Triangle tri = new Triangle(Qtop, v1, v2);
					 triangleList.add(tri);
					 


				 }
				 stack.pop();
				 stack.push(Stop);
				 stack.push(Qtop);
				 
			 } else {
				 
				 while( stack.size() > 1 ){
					 
					 Vertex Svertex1 = (Vertex)stack.peek();
					 stack.pop(); 
					 Vertex Svertex2 = (Vertex)stack.peek();
					 stack.push(Svertex1);
					 
					 
					 double a = getArea(Qtop, Svertex2,Svertex1 );
					 boolean isleft = Svertex1.isOnLeftChain;
					 
					 if( (a > 0 && isleft) || (a < 0 && !isleft ) ){
						 Triangle tri = new Triangle(Qtop, Svertex2, Svertex1);
						 triangleList.add(tri);
						 stack.pop();
					 } else 
						 break;
				 }
				 stack.push(Qtop); 
			 }
			 PQ.poll();
		 }

		 Vertex lastQueuePoint = (Vertex)PQ.peek();
		 Vertex topPoint, top2Point; 
		 while( stack.size() !=1 ){
			 topPoint = (Vertex)stack.peek();
			 stack.pop();
			 top2Point = (Vertex)stack.peek();

			 Triangle tri = new Triangle(lastQueuePoint, topPoint, top2Point);
			 triangleList.add(tri);
			 
			

		 }
		 
		 printTriangles(triangleList);
		 return triangleList;
	 }
 
	 private double getArea(Vertex a, Vertex b, Vertex c){

		  double detleft = (a.getX()-c.getX())*(b.getY()-c.getY());
		  double detright = (a.getY()-c.getY())*(b.getX()-c.getX());

		  return detleft - detright;
		}
	
	 public void  printTriangles(ArrayList<Triangle> triangles) {
			System.out.println("\n Triangles: ");
			for(int i=0;i<triangles.size();i++) {
				Triangle tri = triangles.get(i);
				System.out.println(tri);
			}
			
		}
	 
	public void printVertices() {
		for (Vertex v : vertices) {
			System.out.print(" " + v.getX() + " " + v.getY() + " ");
			System.out.println(v);
		}

	}

	public String toString() {
		return

		" \nvertices: " + verticesToString() + "\n " + this.diagonals.size()
				+ " diagonals: " + diagonalsToString() + " ";
	}

	private String diagonalsToString() {
		String s = " ";
		for (Edge e : this.diagonals) {
			s = s + e.toString() + " ";
		}
		return s;
	}

	private String verticesToString() {
		String s = " ";
		for (Vertex v : this.vertices) {
			s = s + v.toString() + "  ";
		}
		
		return s;
	}
	
	public boolean checkOrientation(TreeSet<Vertex> vertices) {
		Vertex left = (Vertex) vertices.first();
		for (Vertex v : vertices) {
			if (v.getX() <= left.getX()) {
				left = v;
			}
		}
		return liesLeftOfLine(left.getPrevEdge(), left.getNext());
	}

	public TreeSet<Vertex> reverseOrientation(TreeSet<Vertex> vertices) {
		Vertex first = (Vertex) vertices.first();
		Vertex current = first;
		Vertex next;
		do {
			Vertex prev = current.getPrev();
			next = current.getNext();
			current.setNext(prev);
			current.setPrev(next);

			current = next;
		} while (current != first);
		current = first;
		do {
			next = current.getNext();
			Edge newEdge = new Edge(current, next);
			current.setNextEdge(newEdge);
			next.setPrevEdge(newEdge);

			current = next;
		} while (current != first);
		return vertices;
	}

	public boolean liesLeftOfLine(Edge e, Vertex v) {
		int eX = e.getEndVertex().getX() - e.getStartVertex().getX();
		int eY = e.getEndVertex().getY() - e.getStartVertex().getY();
		int mX = v.getX() - e.getStartVertex().getX();
		int mY = v.getY() - e.getStartVertex().getY();

		return eX * mY - eY * mX < 0;
	}

	private class Triangle {
		Vertex a;
		Vertex b;
		Vertex c;
		public Triangle(Vertex a,Vertex b,Vertex c) {
			this.a = a;
			this.b = b;
			this.c = c;
		}
		@Override
		public String toString() {
			String s = " ";
			s += a.toString() + b.toString() + c.toString();
			return s;
		}
	}
	
	private class VertexComparator implements Comparator<Vertex>
	{
	    @Override
	    public int compare(Vertex x, Vertex y)
	    {
	    	if (x.getY() < y.getY()) {
				return 1;
			}
	    	else if (x.getY() > y.getY()) {
				return -1;
			}
			else {
				if(x.getX() > y.getX()) 
					return -1;
				else
					return 1;
			} 
	       
	    }
	}

}
