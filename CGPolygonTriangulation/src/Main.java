import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;




public class Main {

	public static Polygon input() {
		
		BufferedReader br = null;
		
		String file = "inputs/input1.txt";
		Polygon polygon = new Polygon();

		try {
			br = new BufferedReader(new FileReader(file));
			String line;
			
			while((line = br.readLine()) != null) {
				String[] values = line.trim().split("\\s+");
				
				int x = Integer.parseInt(values[0]);
			    int y = Integer.parseInt(values[1]);
			       
			    polygon.addPoint(x, y);
			}
			
		} catch (IOException e){
			e.printStackTrace();
		} finally {
			try {
				if (br != null) br.close();
			} catch (IOException e) {
			}
		}
		return polygon;
	}
	
	
	private void drawPolyGon(final Polygon polygon,final LinkedList<Edge> diagonals) {

        JFrame frame = new JFrame();
        frame.setResizable(false);

        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        
        JPanel panel = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
            	Graphics2D g2 = (Graphics2D) g;
                super.paintComponent(g2);
                g2.setColor(Color.BLUE);
                g2.translate(130, 130);
                g2.scale(6, 6);
                g2.drawPolygon(polygon);
                for(Edge d:diagonals) {
                	g2.setColor(Color.green);
                	g2.drawLine(d.getStartVertex().getX(), d.getStartVertex().getY(), d.getEndVertex().getX(), d.getEndVertex().getY());
                }
            }

            @Override
            public Dimension getPreferredSize() {
                return new Dimension(350, 350);
            }
        };
        frame.add(panel);
        frame.pack();
        frame.setVisible(true);

    }
	
	
	private void drawMonotonePolyGon(final Polygon polygon) {

        JFrame frame = new JFrame();
        frame.setResizable(false);

        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        
        JPanel panel = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
            	Graphics2D g2 = (Graphics2D) g;
                super.paintComponent(g2);
                g2.setColor(Color.BLUE);
                g2.translate(130, 130);
                g2.scale(5, 5);
                g2.drawPolygon(polygon);
                
            }

            @Override
            public Dimension getPreferredSize() {
                return new Dimension(350, 350);
            }
        };
        frame.add(panel);
        frame.pack();
        frame.setVisible(true);

    }
	
	
	public void  printPoly(Polygon p) {
		System.out.println("\nMonotone Polygon points: ");
		for(int i=0;i<p.xpoints.length-1;i++) {
			System.out.print("("+p.xpoints[i]+" "+p.ypoints[i]+")");
		}
	
	}
	
	Polygon removezz(Polygon p) {
		Polygon po = new Polygon();
		
		int[] xPoints = p.xpoints;
		 int[] yPoints = p.ypoints;
		 for(int i=0;i<xPoints.length;i++) {
			 if(xPoints[i]!=0 && yPoints[i]!=0) {
				 po.addPoint(xPoints[i], yPoints[i]);
			 }
		 }
		 return po;
	}
	
	public static void main(String[] args) {
		
		Polygon poly = input();
		
		Main main = new Main();
		//main.printPoly(poly);
		//System.out.println(poly.npoints);
		MonotonePolygon mp = new MonotonePolygon();
		try {
			mp.initialize(poly);
			
			main.drawPolyGon(poly,mp.getDiagonals());
			System.out.println(mp);
			ArrayList<Polygon> polys = mp.getMonotonePolygons();
			for(Polygon p: polys) {
				main.printPoly(p);
				mp.triangulateMonotone(p);
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
}
