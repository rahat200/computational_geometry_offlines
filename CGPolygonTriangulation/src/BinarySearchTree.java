

public class BinarySearchTree {
	
	private class Node {
		private Edge e;
		private Node left;
		private Node right;

		public Node(Edge data) {
			this(data, null, null);
		}

		public Node(Edge data, Node l, Node r) {
			this.left = l;
			this.right = r;
			this.e = data;
		}

		public String toString() {
			return this.e.toString();
		}

		public Edge getEdge() {
			return this.e;
		}

		public void setEdge(Edge e) {
			this.e = e;
		}
	}
	
	
	private Node root;

	public BinarySearchTree() {
		this.root = null;
	}


	public void insert(Edge data) {
		this.root = insert(this.root, data, data.getStartVertex().getY());
	}

	public void insert(Edge data, int y) {
		this.root = insert(this.root, data, y);
	}

	private Node insert(Node p, Edge toInsert, int y) {
		if (p == null) {
			return new Node(toInsert);
		}
		if (compareAtY(toInsert, p.getEdge(), y) == 0) {
			return p;
		}
		if (compareAtY(toInsert, p.getEdge(), y) < 0) {
			p.left = insert(p.left, toInsert, y);
		} else {
			p.right = insert(p.right, toInsert, y);
		}
		return p;
	}

	public Edge searchEdge(Vertex v) {
		return searchEdgeAtX(v.getX(), v.getY());
	}

	private Edge searchEdgeAtX(int x, int y) {
		return searchEdgeAtX(this.root, x, y);
	}

	private Edge searchEdgeAtX(Node p, int x, int y) {
		if (p == null) {
			return null;
		}
		if (calculateXatY(p.getEdge(), y) > x) {
			return searchEdgeAtX(p.left, x, y);
		}
		Edge rightSearch = searchEdgeAtX(p.right, x, y);
		if (rightSearch == null) {
			return p.getEdge();
		}
		return rightSearch;
	}

	public void delete(Edge toDelete) {
		
		this.root = delete(this.root, toDelete, toDelete.getEndVertex().getY());
	}

	private Node delete(Node p, Edge toDelete, int y) {
		if (p == null) {
			throw new AssertionError("No edge to delete found.");
		}
		if (p.getEdge().equals(toDelete)) {
			if (p.left == null) {
				return p.right;
			}
			if (p.right == null) {
				return p.left;
			}
			if (height(p.left) >= height(p.right)) {
				p.setEdge(getRightMostElement(p.left));
				p.left = delete(p.left, p.getEdge(), y);
			} else {
				p.setEdge(getLeftMostElement(p.right));
				p.right = delete(p.right, p.getEdge(), y);
			}
			return p;
		}
		if (compareAtY(toDelete, p.getEdge(), y) < 0) {
			p.left = delete(p.left, toDelete, y);
		} else if (compareAtY(toDelete, p.getEdge(), y) > 0) {
			p.right = delete(p.right, toDelete, y);
			Double.compare(y, y);
		}
		return p;
	}

	
	public  double calculateXatY(Edge e, double y) {
		Vertex a = e.getStartVertex();
		Vertex b = e.getEndVertex();
		double m = (y - a.getY()) / (b.getY() - a.getY());
		//System.out.println(a.getX() + m * (b.getX() - a.getX()));
		return a.getX() + m * (b.getX() - a.getX());
	}
	
	private int compareAtY(Edge one, Edge two, int y) {
		return Double.compare(calculateXatY(one, y),
				calculateXatY(two, y));
	}
	
	
	private Edge getRightMostElement(Node p) {
		while (p.right != null) {
			p = p.right;
		}
		return p.getEdge();
	}

	private Edge getLeftMostElement(Node p) {
		while (p.left != null) {
			p = p.left;
		}
		return p.getEdge();
	}

	public BinarySearchTree clone() {
		BinarySearchTree twin = new BinarySearchTree();

		twin.root = cloneHelper(this.root);

		return twin;
	}

	private Node cloneHelper(Node p) {
		if (p == null) {
			return null;
		}
		return new Node(p.getEdge().clone(), cloneHelper(p.left),
				cloneHelper(p.right));
	}

	public int height() {
		return height(this.root);
	}

	private int height(Node p) {
		if (p == null) {
			return -1;
		}
		//System.out.println(Math.max(height(p.left), height(p.right)));
		return 1 + Math.max(height(p.left), height(p.right));
	}

	public int size() {
		return size(this.root);
	}

	private int size(Node p) {
		if (p == null) {
			return 0;
		}
		return 1 + size(p.left) + size(p.right);
	}

	public int countLeaves() {
		return countLeaves(this.root);
	}

	private int countLeaves(Node p) {
		if (p == null) {
			return 0;
		}
		if ((p.left == null) && (p.right == null)) {
			return 1;
		}
		return countLeaves(p.left) + countLeaves(p.right);
	}

	public int width() {
		int max = 0;
		for (int i = 0; i <= height(); i++) {
			int tmp = width(this.root, i);
			if (tmp > max) {
				max = tmp;
			}
		}
		return max;
	}

	private int width(Node p, int depth) {
		if (p == null) {
			return 0;
		}
		if (depth == 0) {
			return 1;
		}
		return width(p.left, depth - 1) + width(p.right, depth - 1);
	}


}
