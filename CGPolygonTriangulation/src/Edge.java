
public class Edge implements Comparable<Edge> {
	private Vertex start;
	private Vertex end;
	private Vertex helper;

	public Edge(Vertex start, Vertex end) {
	
		this.start = start;
		this.end = end;
	}


	public Vertex getHelper() {
		return this.helper;
	}

	public void reverse() {
		Vertex tmp = this.start;
		this.start = this.end;
		this.end = tmp;
	}

	public void setHelper(Vertex helper) {
		releaseHelper();
		this.helper = helper;
		helper.setHelpedEdge(this);
	}

	public Vertex releaseHelper() {
		Vertex oldHelper = getHelper();
		if (oldHelper != null) {
			oldHelper.setNotHelping();
		}
		return oldHelper;
	}

	public Vertex getStartVertex() {
		return this.start;
	}

	public Vertex getEndVertex() {
		return this.end;
	}

	public Edge clone() {
		Edge cloned = new Edge(getStartVertex(), getEndVertex());
		if (getHelper() != null) {
			cloned.setHelper(getHelper().clone());
		}
		return cloned;
	}

	public String toString() {
		String s = " " + this.start.toString() + "-----------"
				+ this.end.toString();
		if (this.helper != null) {
			s = s + ", helper=" + this.helper;
		}
		return s;
	}

	public int compareTo(Edge o) {
		return 0;
	}
}
