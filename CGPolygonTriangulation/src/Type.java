public enum Type {

	START, END, SPLIT, MERGE, REGULAR_LEFT, REGULAR_RIGHT;

	public String toString() {
		if (this == START) {
			return "Start ";
		}
		if (this == END) {
			return "End ";
		}
		if (this == SPLIT) {
			return "Split ";
		}
		if (this == MERGE) {
			return "Merge ";
		}
		if (this == REGULAR_LEFT) {
			return "RegularLeft ";
		}
		return "RegularRight ";
	}
}
