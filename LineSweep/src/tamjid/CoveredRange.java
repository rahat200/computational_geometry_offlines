package tamjid;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CoveredRange {

	List<Line> lines = new ArrayList<Line>();
	List<EventPoint> events = new ArrayList<EventPoint>();
	
	public static void main(String[] args) {
		CoveredRange obj = new CoveredRange();
		obj.input();
		obj.getCoveredPosition();

	}
	
	public void getCoveredPosition() {
		int lastEventpoint = events.get(0).position;
		int curCoveredLineCount = 0;
		int totalCoveredArea = 0;
		
		Collections.sort(events);
		
		for(EventPoint event : events) {
			int curEventPoint = event.position;
			
			if(curCoveredLineCount >= 1) {
				totalCoveredArea += curEventPoint-lastEventpoint;
			}
			
			lastEventpoint = curEventPoint;
			
			if(event.isStart) {
				curCoveredLineCount++;
			}
			else {
				curCoveredLineCount--;
			}
		}
		
		System.out.println("total covered area: "+totalCoveredArea);
		
	}
	
	class Line {
		int start;
		int end;
		
		Line(int start, int end) {
			this.start = start;
			this.end = end;
		}
	}
	
	class EventPoint implements Comparable<EventPoint> {
		int position;
		int lineNo;
		boolean isStart;
		
		public EventPoint(int pos,int line, boolean isstart) {
			this.position = pos;
			this.lineNo = line;
			this.isStart = isstart;
		}

		@Override
		public int compareTo(EventPoint o) {
			
			if(position != o.position) {
				return position-o.position;
			}
			else if(isStart != o.isStart) {
				if(isStart)
					return -1;
				else
					return 1;
			}
			else {
				return lineNo - o.lineNo;
			}	
		}
		
	}
	
	public void input() {
		BufferedReader br = null;
		
		String file = "inputs/input1.txt";

		try {
			br = new BufferedReader(new FileReader(file));
			String line;
			int lineCount = 0;
			while((line = br.readLine()) != null) {
				lineCount++;
				
				String[] values = line.trim().split("\\s+");
				int start = Integer.parseInt(values[0]);
				int end = Integer.parseInt(values[1]);
				
				Line ln = new Line(start,end);
				lines.add(ln);
				
				events.add(new EventPoint(ln.start, lineCount, true));
				events.add(new EventPoint(ln.end, lineCount, false));
			}
			
		} catch (IOException e){
			e.printStackTrace();
		} finally {
			try {
				if (br != null) br.close();
			} catch (IOException e) {
			}
		}
	}
	

}
