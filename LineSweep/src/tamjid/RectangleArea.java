package tamjid;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;



public class RectangleArea {

	List<Rectangle> rectangles = new ArrayList<Rectangle>();
	List<EventPointHorizontal> events_horz = new ArrayList<EventPointHorizontal>();
	List<EventPointVertical> events_vert = new ArrayList<EventPointVertical>();
	
	int RectNo;
	int[] active_rect;
	
	public static void main(String[] args) {
		RectangleArea obj = new RectangleArea();
		obj.input();
		obj.UnionRectangleArea();

	}
	
	public void UnionRectangleArea() {
		
		Collections.sort(events_vert);
		Collections.sort(events_horz);
		
		active_rect = new int[RectNo];
		for(int i=0;i<RectNo;i++) {
			active_rect[i] = 0;
		}
		int total_area=0;
		int lastx = events_vert.get(0).x;
		
		for(EventPointVertical v_event:events_vert) {
			
			if(v_event.isStart==true && active_rect[v_event.rec_no-1] == 0) {
				active_rect[v_event.rec_no-1] = 1; //active the rectangle
			}
			if(v_event.isStart==false && active_rect[v_event.rec_no-1] == 1) {
				active_rect[v_event.rec_no-1] = 0; //active the rectangle
			}
			
			int currentx = v_event.x;
			total_area += (currentx-lastx)*getCutLength(active_rect);
		}
		System.out.println("area:"+total_area);
	}
	
	public int getCutLength(int[] activeset){
		int total_cut_length = 0;
		int lasty=0;
		for(int i=0;i<events_horz.size();i++) {
			if(activeset[events_horz.get(i).rec_no-1] == 1) {
				lasty = events_horz.get(i).y;
			}
		}
		
		for(EventPointHorizontal h_event: events_horz) {
			if(activeset[h_event.rec_no-1] == 1) { //if rectangle is active
				int currenty = h_event.y;
				total_cut_length += currenty;
				lasty = currenty;
			}
			
		}
		return total_cut_length;
	}

	class Rectangle {
		int x1;
		int y1;
		int x2;
		int y2;
		
		public Rectangle(int x1,int y1,int x2,int y2) {
			this.x1 = x1;
			this.y1 = y1;
			
			this.x2 = x2;
			this.y2 = y2;
		}
	}
	
	class EventPointHorizontal implements Comparable<EventPointHorizontal> {
		int rec_no;
		boolean isStart;
		
		int x1,x2,y;
		
		public EventPointHorizontal(int x1,int x2,int y,boolean isstart,int recNo) {
			this.x1 = x1;
			this.x2 = x2;
			this.y = y;
			this.isStart = isstart;
			this.rec_no = recNo;
		}

		@Override
		public int compareTo(EventPointHorizontal o) {
			
			return y-o.y;	
		}
		
	}
	class EventPointVertical implements Comparable<EventPointVertical> {
		int rec_no;
		boolean isStart;
		
		int y1,y2,x;
		
		public EventPointVertical(int y1,int y2,int x,boolean isStart, int recNo) {
			this.y1 = y1;
			this.y2 = y2;
			this.x = x;
			this.isStart = isStart;
			this.rec_no = recNo;
		}
		
		@Override
		public int compareTo(EventPointVertical o) {
			
			return x-o.x;	
		}
		
	}
	
	public void input() {
		BufferedReader br = null;
		
		String file = "inputs/input1.txt";

		try {
			br = new BufferedReader(new FileReader(file));
			String line;
			int recCount = 0;
			while((line = br.readLine()) != null) {
				recCount++;
				
				String[] values = line.trim().split("\\s+");
				int x1 = Integer.parseInt(values[0]);
				int x2 = Integer.parseInt(values[1]);
				int y1 = Integer.parseInt(values[2]);
				int y2 = Integer.parseInt(values[3]);
				
				Rectangle rect = new Rectangle(x1, y1, x2, y2);
				rectangles.add(rect);
				
				events_vert.add(new EventPointVertical(y1, y2, x1, true,recCount));
				events_vert.add(new EventPointVertical(y1, y2, x2, false,recCount));
				
				events_horz.add(new EventPointHorizontal(x1, x2, y1, true,recCount));
				events_horz.add(new EventPointHorizontal(x1, x2, y2, false,recCount));
				
			}
			RectNo = recCount;
			
		} catch (IOException e){
			e.printStackTrace();
		} finally {
			try {
				if (br != null) br.close();
			} catch (IOException e) {
			}
		}
	}
}
